#!/usr/bin/env bash

WORKSPACE_PATH=$PWD/datacube_workspace

docker run --name datacubeserver \
            -d \
            -p 8081:8081 \
            -v $WORKSPACE_PATH:/data \
            idocias/datacubeserver:latest

echo "DataCube Server API available at : http://localhost:8081"