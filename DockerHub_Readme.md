
# DataCube Server -  Docker image

This image contains the [Datacube Server](https://github.com/MizarWeb/DataCubeServer) app that provides an API to get informations from cubes files of spectral data.
This API is used by [Datacube](https://github.com/MizarWeb/DataCube).

It also creates the default folders for .fits and .nc files and downloads the cube 1342228703_M17-2_SPIRE-FTS_15.0.3244_HR_SLW_gridding_cube.fits from [HERSCHEL](http://idoc-herschel.ias.u-psud.fr/sitools/client-user/Herschel/project-index.html) that is opened at startup.
These folders can be replaced with a host one using the -v option to bind-mount one to /data as written in the following script that you can use to run a container.

## Usage

The script  [run.sh](https://git.ias.u-psud.fr/mizar/docker_datacubeserver/blob/master/run.sh) will create a local folder that can host your cubes to be read by the server run a new container from this image.

The DataCube Server API available at : http://localhost:8081.

NOTE: The authentication has been disabled in this Server's image waiting for it's implementation to be fixed in the project. 

## Logging
To access the container's logs do :

```bash
docker logs datacubeserver
```

## Cleanup

To discard your container run the script [clean.sh](https://git.ias.u-psud.fr/mizar/docker_datacubeserver/blob/master/clean.sh) 
