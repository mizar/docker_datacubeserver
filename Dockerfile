FROM openjdk:8-alpine
ADD VERSION .

# Proxy runtime
ARG HTTP_PROXY
ARG HTTPS_PROXY

ENV http_proxy=$HTTP_PROXY
ENV https_proxy=$HTTPS_PROXY

# ------ BACKEND

# Install java
RUN echo "deb http://deb.debian.org/debian stretch-backports main" >> /etc/apt/sources.list.d/nginx.list
RUN apt-get update && apt-get -y dist-upgrade
RUN apt-get -y -t stretch-backports install openjdk-8-jdk

# install maven
RUN apt-get -y install maven

# clone Server
WORKDIR /opt/ 
RUN git clone  --single-branch --branch nologinoption https://github.com/mmebsout/DataCubeServer.git \
    && cd DataCubeServer \
    # edit properties : .fits and .nc are found in "/data/private" and "/data/public" folders
    && sed -i -e '/workspace=/ s/=.*/=\/data\//' -e '/workspace_cube=/ s/=.*/=\/data\//' cubeExplorer.properties \
    && sed -i -e '/workspace=/ s/=.*/=\/data\//' -e '/workspace_cube=/ s/=.*/=\/data\//' src/main/resources/conf/cubeExplorer.properties \
    # install server 
    && mvn clean install 

# create /data/private and /data/public folders 
RUN mkdir -p /data/private && mkdir /data/public \
    && chmod +x /data/private && chmod +x /data/public \
    # download test cube
    && wget -P  /data/public http://idoc-herschel.ias.u-psud.fr/sitools/datastorage/user/storageRelease/R6_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/HH_IR_int-2/HH_IR_int-2_1342228508_L2_red_145.52_OI3P0-3P1_ProjectedCube.fits

EXPOSE 8081

# ------- RUN APPs

COPY ./script.sh /
RUN chmod +x /script.sh
ENTRYPOINT ["/script.sh"]
